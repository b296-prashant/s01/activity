name = "John"
age = 38
occupation = "writer"
movie = "One more chance"
rating = 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %")

num1 = 3
num2 = 4
num3 = 5

print(num1*num2)
print(num1<num3)
print(num2+num3)
